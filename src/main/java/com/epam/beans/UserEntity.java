package com.epam.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Anoop_Dande
 *
 */
@ApiModel(description = "All details about the User.")
@Getter
@Setter
@AllArgsConstructor
@ToString
@NoArgsConstructor
@Entity
public @Data class UserEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false)
	@Size(min = 2, max = 100)
	@NotBlank(message = "First name must not be blank")
	private String firstName;

	@Column(nullable = false)
	@Size(min = 2, max = 100)
	@NotBlank(message = "Last name must not be blank")
	private String lastName;

	@Column(nullable = false, unique = true)
	@Size(min = 2, max = 100)
	@NotBlank(message = "Email must not be blank")
	private String email;

}