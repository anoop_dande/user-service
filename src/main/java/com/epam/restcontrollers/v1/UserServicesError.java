package com.epam.restcontrollers.v1;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.epam.exceptions.UserNotFoundException;

@ControllerAdvice
public class UserServicesError {

	/**
	 * @param exception
	 * @returns the handled exception message.
	 */
	@ExceptionHandler({ UserNotFoundException.class })
	public ResponseEntity<Object> handleUserNotFoundException(UserNotFoundException exception) {
		ErrorDTO error = new ErrorDTO(HttpStatus.NOT_FOUND, exception.getMessage());
		return new ResponseEntity<>(error, new HttpHeaders(), error.getStatusOfError());
	}
	

}
