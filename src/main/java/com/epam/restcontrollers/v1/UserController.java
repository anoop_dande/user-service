package com.epam.restcontrollers.v1;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.epam.dto.UserDTO;
import com.epam.services.UserServices;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Anoop_Dande
 *
 */
@RefreshScope
@RestController
@RequestMapping("users")
@Api(value = "This is User service. This api can be used to add, update, read and delete the user")
@ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully added user"),
		@ApiResponse(code = 200, message = "The request has been succeeded "),
		@ApiResponse(code = 500, message = "Internal Server error"),
		@ApiResponse(code = 404, message = "The resource you are looking for is not found"),
		@ApiResponse(code = 400, message = "The requested resource is not allowed") })
public class UserController {

	@Autowired
	UserServices userServices;

	@ApiOperation(value = "Add a new user", response = UserDTO.class)
	@PostMapping
	public ResponseEntity<UserDTO> addUser(
			@ApiParam(value = "User model that needs to be added", required = true) @Valid @RequestBody UserDTO userDTO) {
		final URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(userDTO.getId())
				.toUri();
		return ResponseEntity.created(uri).body(userServices.addUser(userDTO));
	}

	@ApiOperation(value = "Find all Users", response = UserDTO.class, responseContainer = "List")
	@GetMapping
	public ResponseEntity<List<UserDTO>> getAllUsers() {
		return ResponseEntity.ok(userServices.getAllUsersList());
	}

	@ApiOperation(value = "Update a existing user", response = UserDTO.class)
	@PutMapping("{userId}")
	public ResponseEntity<UserDTO> update(
			@ApiParam(value = "UserId to update the user", required = true) @PathVariable Long userId,
			@ApiParam(value = "User model that needs to be added", required = true) @Valid @RequestBody UserDTO user) {
		return ResponseEntity.ok(userServices.updateUser(userId, user));
	}

	@ApiOperation(value = "find a User by userId", response = UserDTO.class)
	@GetMapping("{userId}")
	public ResponseEntity<UserDTO> getUserById(
			@ApiParam(value = "UserId to update the user", required = true) @PathVariable Long userId) {
		return ResponseEntity.ok(userServices.findUserById(userId));
	}

	@ApiOperation(value = "Delete a User by userId", response = UserDTO.class)
	@DeleteMapping("{userId}")
	public ResponseEntity<?> deleteUser(
			@ApiParam(value = "UserId to update the user", required = true) @PathVariable Long userId) {
		userServices.deleteUserById(userId);
		return ResponseEntity.noContent().build();
	}

}
