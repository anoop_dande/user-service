package com.epam.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.beans.UserEntity;
import com.epam.dto.UserDTO;
import com.epam.exceptions.UserNotFoundException;
import com.epam.repository.UserRepository;

@Service
public class UserServicesImpl implements UserServices {

	private static final Logger logger = LogManager.getLogger(UserServicesImpl.class);

	private UserRepository userRepository;

	/**
	 * @param userRepository
	 */
	@Autowired
	public UserServicesImpl(UserRepository userRepository) {
		super();
		this.userRepository = userRepository;
	}

	/**
	 * returns all the existed users
	 */
	@Override
	public List<UserDTO> getAllUsersList() {
		List<UserEntity> userEntityList = userRepository.findAll();
		if (userEntityList.isEmpty())
			throw new UserNotFoundException("users not found");
		List<UserDTO> userDTOList = new ArrayList<>();
		userEntityList.forEach(userEntity -> userDTOList.add(new UserDTO(userEntity.getId(), userEntity.getFirstName(),
				userEntity.getLastName(), userEntity.getEmail())));
		logger.info("fetch all the users list.");
		return userDTOList;
	}

	/**
	 * returns specific user.
	 */
	@Override
	public UserDTO findUserById(Long userId) {
		UserEntity userEntity = userRepository.findById(userId)
				.orElseThrow(() -> new UserNotFoundException("selected user not available"));
		logger.info("found the user with the id {}", userId);
		return new UserDTO(userEntity.getId(), userEntity.getFirstName(), userEntity.getLastName(),
				userEntity.getEmail());
	}

	/**
	 * returns the user which has been added
	 */
	@Override
	public UserDTO addUser(UserDTO userDTO) {
		UserDTO userDTOAdded;
		try {
			UserEntity userEntity = convertDtoToEntity(userDTO);
			userEntity = userRepository.save(userEntity);
			userDTOAdded = new UserDTO(userEntity.getId(), userEntity.getFirstName(), userEntity.getLastName(),
					userEntity.getEmail());
			logger.info("added user successfully");
		} catch (NullPointerException e) {
			throw new UserNotFoundException("Unable to add User");
		}
		return userDTOAdded;
	}

	/**
	 * @param userDTO
	 * @return
	 */
	private UserEntity convertDtoToEntity(UserDTO userDTO) {
		return new UserEntity(userDTO.getId(), userDTO.getFirstName(), userDTO.getLastName(), userDTO.getEmail());
	}

	/**
	 * deletes the user.
	 */
	@Override
	public void deleteUserById(Long userId) {
		userRepository.deleteById(userId);
		logger.info("deleted user with id {}", userId);
	}

	/**
	 * returns the user which was updated
	 */
	@Override
	public UserDTO updateUser(Long userId, UserDTO userDTO) {
		UserEntity userEntity = convertDtoToEntity(userDTO);
		userRepository.findById(userId).map(legacyUser -> userRepository.save(userEntity))
				.orElseThrow(() -> new UserNotFoundException("User not found"));
		logger.info("updated user with id {}", userId);
		return new UserDTO(userEntity.getId(), userEntity.getFirstName(), userEntity.getLastName(),
				userEntity.getEmail());
	}

}
