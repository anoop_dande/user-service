package com.epam.services;

import java.util.List;

import com.epam.dto.UserDTO;

public interface UserServices {
	public List<UserDTO> getAllUsersList();

	public UserDTO findUserById(Long bookId);

	public UserDTO addUser(UserDTO userToBeAdded);

	public void deleteUserById(Long bookId);

	public UserDTO updateUser(Long userId, UserDTO userDTO);

}