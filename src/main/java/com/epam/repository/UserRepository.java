package com.epam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.epam.beans.UserEntity;

/**
 * @author Anoop_Dande
 *
 */
@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
}
