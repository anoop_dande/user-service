package com.epam.restcontrollers;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.epam.dto.UserDTO;
import com.epam.restcontrollers.v1.UserController;
import com.epam.services.UserServicesImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

class UserControllerTest {

	private MockMvc mockMvc;

	@Mock
	UserServicesImpl userServices;

	@InjectMocks
	UserController userController;

	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
	}

	static UserDTO userDTO;
	static List<UserDTO> users;

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@BeforeAll
	public static void initVars() {
		userDTO = new UserDTO(1L, "Anoop", "Dande", "anoop_dande@epam.com");
		users = new ArrayList<>();
		users.add(userDTO);
	}

	@Test
	void testGetAllUsers() throws Exception {
		when(userServices.getAllUsersList()).thenReturn(users);
		this.mockMvc.perform(get("/users")).andExpect(status().isOk());
	}

	@Test
	void testGetUserById() throws Exception {
		when(userServices.findUserById(userDTO.getId())).thenReturn(userDTO);
		this.mockMvc.perform(get("/users/{userId}", userDTO.getId())).andExpect(status().isOk());
	}

	@Test
	void testDeleteById() throws Exception {
		doNothing().when(userServices).deleteUserById(userDTO.getId());
		this.mockMvc.perform(delete("/users/{userId}", userDTO.getId())).andExpect(status().is2xxSuccessful());
	}

	@Test
	void testUpdateById() throws Exception {
		when(userServices.updateUser(userDTO.getId(), userDTO)).thenReturn(userDTO);
		this.mockMvc.perform(
				put("/users/{userId}", userDTO.getId()).contentType("application/json").content(asJsonString(userDTO)))
				.andExpect(status().isOk());
	}

	@Test
	void testAddUser() throws Exception {

		when(userServices.addUser(userDTO)).thenReturn(userDTO);
		this.mockMvc.perform(post("/users").contentType("application/json").content(asJsonString(userDTO)))
				.andExpect(status().isCreated());
	}

}