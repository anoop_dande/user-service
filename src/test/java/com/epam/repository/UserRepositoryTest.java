/*package com.epam.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.epam.beans.UserEntity;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
class UserRepositoryTest {

	@Autowired
	UserRepository userRepository;

	static UserEntity user;

	@BeforeAll
	public static void initVars() {
		user = new UserEntity();
		user.setId(1L);
		user.setFirstName("Anoop");
		user.setLastName("Dande");
		user.setEmail("anoop_dande@gmail.com");
	}

	@Test
	public void testSave() {
		userRepository.save(user);
		assertEquals("Anoop", user.getFirstName());
	}

	@Test
	void testFindAll() {
		userRepository.save(user);
		List<UserEntity> userList = userRepository.findAll();
		assertThat(userList).isNotEmpty();
	}

	@Test
	void testFindById() {
		userRepository.save(user);
		assertEquals("Anoop", userRepository.findById(1L).get().getFirstName());
	}

	@Test
	void testDelete() {
		userRepository.save(user);
		userRepository.deleteById(1L);
		assertThat(userRepository.findById(1L)).isEmpty();
	}

}*/
