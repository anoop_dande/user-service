package com.epam.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.epam.beans.UserEntity;
import com.epam.dto.UserDTO;
import com.epam.repository.UserRepository;

class UserServicesImplTest {

	@Mock
	UserServices userServices;

	@InjectMocks
	UserServicesImpl userServicesImpl;

	@Mock
	UserRepository userRepository;

	static UserEntity user;
	static UserDTO userDTO;
	static List<UserEntity> userList;

	@BeforeEach
	void init() {
		MockitoAnnotations.initMocks(this);
	}

	@BeforeAll
	public static void initVars() {
		user = new UserEntity(1L, "Anoop", "Dande", "anoop_dande@epam.com");
		userDTO = new UserDTO(1L, "Anoop", "Dande", "anoop_dande@epam.com");
		userList = new ArrayList<>();
		userList.add(user);
	}

	@Test
	void testGetAllUsersList() {
		when(userRepository.findAll()).thenReturn(userList);
		assertNotNull(userServicesImpl.getAllUsersList());
	}

	@Test
	void testFindUserById() {
		Optional<UserEntity> optionalUser = Optional.ofNullable(user);
		when(userRepository.findById(1L)).thenReturn(optionalUser);
		assertEquals(userDTO, userServicesImpl.findUserById(1L));
	}

	@Test
	void testAddUser() {
		when(userRepository.save(Mockito.any())).thenReturn(user);
		assertEquals(userDTO, userServicesImpl.addUser(userDTO));
	}

	@Test
	void testDeleteUserById() {
		doNothing().when(userServices).deleteUserById(1L);
		userServicesImpl.deleteUserById(1L);
		verify(userRepository, times(1)).deleteById(1L);
	}

	@Test
	void testUpdateUser() {
		UserEntity previousUser = new UserEntity(1L, "Anoop Kumar", "Dande", "anoop_dande@epam.com");
		Optional<UserEntity> optionalUser = Optional.ofNullable(previousUser);
		when(userRepository.findById(1L)).thenReturn(optionalUser);
		when(userRepository.save(user)).thenReturn(user);
		assertEquals(userDTO, userServicesImpl.updateUser(1L, userDTO));
	}

}
